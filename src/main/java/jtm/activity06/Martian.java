package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {

	int weight;
	Object stomach;

	public Martian() {
		weight = Alien.BirthWeight;
	}

	@Override
	public void eat(Object item) {
		if (this.stomach == null) {
			this.stomach = item;
			if (item instanceof Humanoid) {
				Humanoid temp = (Humanoid) item;
				this.weight += temp.getWeight();
				temp.killHimself();
			}
			if (item instanceof Integer) {
				Integer temp = (Integer) item;
				this.weight += temp;
			}
		}
	}

	@Override
	public void eat(Integer food) {
		eat((Object) food);
	}

	@Override
	public Object vomit() {
		this.weight = Alien.BirthWeight;

		if (stomach instanceof Integer && (Integer) this.stomach != 0) {
			Integer tempInt = (Integer) this.stomach;
			this.stomach = null;
			return tempInt;
		}
		if (stomach instanceof Integer) {
			return this.weight;
		}
		if (stomach == null)
			return null;
		Object temp = this.stomach;
		this.stomach = null;
		return temp;
	}

	@Override
	public String isAlive() {
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		return isAlive();
	}

	@Override
	public int getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ": " + weight + " [" + stomach + "]";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return clone(this);
	}

	private Object clone (Object current) {

		if (current instanceof Integer) //Martian ate food
			return current;// return food
		
		if (current instanceof Martian) {// Martian ate Martian
			Martian theMartian = (Martian) current; 
			Martian newMartian = new Martian();
			newMartian.weight = theMartian.getWeight();
			newMartian.stomach = clone(theMartian.stomach);
			System.out.println(newMartian.stomach + ", " + theMartian.stomach);
			return newMartian; //return Martian with all eaten Martians
		}
		if (current instanceof Human) { //Martian ate Human
			Human theHuman = (Human) current;
			Human newHuman = new Human();
			newHuman.weight = theHuman.weight;
			newHuman.stomach = theHuman.getWeight() - theHuman.BirthWeight;
			return newHuman; // return Human with all eaten food
		}
		else return null; // Martian didn't eat
	}

	public static void main(String[] args) {
		Martian m = new Martian();
		Martian h = new Martian();
		h.eat(6);
		System.out.println("small martian ate 6. He became " + h);
		m.eat(h);
		System.out.println("big martian ate small martian. His weight is " + m.weight + ". He became " + m);
		//Martian anotherMartian = (Martian) m.clone(h);
		try {
			Martian newMartian = (Martian) m.clone();
			System.out.println("cloned stomach is " + newMartian.stomach);
			System.out.println("cloned Martian is " + newMartian);
		} catch (CloneNotSupportedException ex) {
			ex.printStackTrace();
		}
	}

}
