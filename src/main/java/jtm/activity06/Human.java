package jtm.activity06;

public class Human implements Humanoid {

	int stomach; // store eaten item
	int weight;
	// int eatenItem;
	// int birthWeight;
	boolean isAlive;

	public Human() {
		isAlive = true;
		stomach = 0;
		// eatenItem = 0;
		weight = BirthWeight;
	}

	@Override
	public void eat(Integer food) {
		//System.out.println("human wants to eat. His stomach is " + stomach);
		if (this.stomach == 0) {
			//System.out.println("human is eating " + food + " food");
			this.stomach = food;
			this.weight += stomach;
		} else {
			//System.out.println("so he doesn't eat");
			//this.stomach = 0;
		}
		this.weight = getWeight();
	}

	@Override
	public Integer vomit() {
		//System.out.println("human wants to vomit. His stomach is " + stomach);
		int vomittedEat = 0;
		if (this.stomach != 0) {
			//System.out.println("human is vomitting " + stomach + " food");
			vomittedEat = this.stomach;
			this.weight -= stomach;
			this.stomach = 0;
		} else {
			//System.out.println("human isn't vomitting");
		}
		return vomittedEat;
	}

	@Override
	public String isAlive() {
		String status = "Alive";
		if (!isAlive)
			status = "Dead";
		// return "Dead";
		return status;
	}

	@Override
	public String killHimself() {
		isAlive = false;
		return isAlive();
	}

	@Override
	public int getWeight() {
		//System.out.println("stomach is " + stomach);
		//weight = this.weight + this.stomach;
		//System.out.println("so his weight is " + weight);
		return weight;
	}

	/// @return value of Humanoid in form "ImplementingType: weight
	// * [contentOfstomach]";

	public String toString() {
		return this.getClass().getSimpleName() + ": " + this.getWeight() + " [" + stomach + "]";
	}

	public static void main(String[] args) {
		Human h = new Human();
		h.eat(5);
		h.vomit();
		h.vomit();
		h.eat(1);
	}

}
