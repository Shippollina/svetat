
package jtm.extra04;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StudentTest {

	private static Logger logger = Logger.getLogger(StudentTest.class);

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link main.java.jtm.extra04.Student#Student(int, java.lang.String, java.lang.String, int)}
	 * .
	 */
	@Test
	public final void testStudentIntStringStringInt() {
		Student testCase = new Student(1, "Bob", "Bobson", 27123432);
		try {
			assertEquals("Student value comparison error.", 1, testCase.getID());
			assertEquals("Student value comparison error.", "Bob", testCase.getFirstName());
			assertEquals("Student value comparison error.", "Bobson", testCase.getLastName());
			assertEquals("Student value comparison error.", 27123432, testCase.getPhoneNumber());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);

		}
	}

}
