package jtm.activity13;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn;

	public TeacherManager() {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "student"
		// pass = "Student007"
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods

		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root", "");
			conn.setAutoCommit(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"

		// String firstName = null;
		// String lastName = null;
		// int iD = 0;
		Teacher theTeacher = new Teacher(0, null, null);
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM database_activity.Teacher where id = ?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				theTeacher = new Teacher(id, rs.getString(2), rs.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return theTeacher;
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!

		List<Teacher> teacherList = new ArrayList<Teacher>();
		try {
			PreparedStatement ps = conn.prepareStatement(
					"SELECT * FROM database_activity.Teacher WHERE firstname LIKE ? AND lastname LIKE ?");
			ps.setString(1, "%" + firstName + "%");
			ps.setString(2, "%" + lastName + "%");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				teacherList.add(new Teacher(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return teacherList;
	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.
		boolean status = false;

		try {
			PreparedStatement s = conn
					.prepareStatement("INSERT INTO database_activity.Teacher(firstname, lastname) VALUES (?,?)");
			s.setString(1, firstName);
			s.setString(2, lastName);
			int count = s.executeUpdate();
			if (count > 0) {
				conn.commit();
				status = true;
			} else status = false;
		} catch (SQLException e) {
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database
		boolean status = false;

		try {
			PreparedStatement stmt = conn.prepareStatement(
					"INSERT INTO database_activity.Teacher (id, firstname, lastname) VALUES (?, ?, ?)");
			stmt.setInt(1, teacher.getId());
			stmt.setString(2, teacher.getFirstName());
			stmt.setString(3, teacher.getLastName());
			int number = stmt.executeUpdate();
			if (number > 0) {
				conn.commit();
				status = true;
			} else status = false;
		} catch (SQLException e) {
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 * @throws SQLException
	 */
	public boolean updateTeacher(Teacher teacher) throws SQLException {
		boolean status = false;
		
		int index = teacher.getId();
		System.out.println(index);

		// TODO #6 Write an sql statement that updates teacher information.
		try {
			PreparedStatement s = conn.prepareStatement(
					"UPDATE database_activity.Teacher set firstname = ?, lastname = ? where id = ?");
			// to update teacher by id you should 1)find a teacher with the same id 2)
			// update values at the entry
			//s.setInt(1, index);
			s.setString(1, teacher.getFirstName());
			s.setString(2, teacher.getLastName());
			s.setInt(3, index);
			int number = s.executeUpdate();
			if (number > 0) {
				conn.commit();
				status = true;
			}
		} catch (SQLException e) {
			conn.rollback();
			e.printStackTrace();
		}
		return status;
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		boolean status = false;
		try {
			PreparedStatement st = conn.prepareStatement("DELETE FROM database_activity.Teacher where id = ?");
			st.setInt(1, id);
			int number = st.executeUpdate();
			if (number > 0)
				status = true;
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	public void closeConnecion() {
		// TODO Close connection to the database server and reset conn object to null
		try {
			if (conn != null)
				conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
