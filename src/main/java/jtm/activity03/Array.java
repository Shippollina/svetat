package jtm.activity03;

import java.util.Arrays;

public class Array {
    static int[] myArray;

    public static void main(String[] args) {
        // TODO Use passed parameters for main method to initialize array
        // Hint: use Run as” Run configurations... Arguments to pass parameters to
        // main method when calling from Eclipse
        // Sort elements in this array in ascending order
        // Hint: use Integer.parseInt(args[n]) to convert passed
        // parameters from String to int
        // Hint: use Arrays.sort(...) from
        // https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html

        Array.myArray = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            Array.myArray[i] = Integer.parseInt(args[i]);
        }

        Arrays.sort(Array.myArray);
        
        Array.printSortedArray();

    }

    public static void printSortedArray() {
        // TODO print content of array on standard output
        // Hint: use Arrays.toString(array) method for this
        System.out.println(Arrays.toString(Array.myArray));
    }

    public static int[] returnSortedArray() {
        // TODO return reference to this array
        return Array.myArray;
    }

}