package jtm.extra01;

public class Zodiac {

	/**
	 * Determine the sign of the zodiac, use day and month.
	 * 
	 * @param day
	 * @param month
	 * @return zodiac
	 */
	
	Zodiac(int day, int month){
		
	}
	
	public static String getZodiac(int day, int month) {
		String zodiac = null;
		// TODO #1: Implement method which return zodiac sign names
		// As method parameter - day and month;
		// Look at wikipedia:
		// https://en.wikipedia.org/wiki/Zodiac#Table_of_dates
		// Tropical zodiac, to get appropriate date ranges for signs
		
		if(month == 1) {
			if(day >= 1 && day < 20) {zodiac = "capricorn";}
			else if(day >=21 && day <= 31) {zodiac = "aquarius";}
			else zodiac = "Please provide correct date";
		} else if (month == 2) {
			if(day >= 1 && day <=19) {zodiac = "aquarius";}
			else if(day >19 && day <=28) {zodiac = "pisces";}
			else zodiac = "Please provide correct date";
		} else if(month == 3) {
			if(day >= 21) {	zodiac = "aries";}
			else if (day < 21) {zodiac = "pisces";}
			else zodiac = "Please provide correct date";
		} else if (month == 4) {
			if(day >= 1 && day <=20) {zodiac = "aries";}
			else if(day > 20 && day <= 30) {zodiac = "taurus";}
			else zodiac = "Please provide correct date";
		} else if (month == 5) {
			if(day >= 1 && day <= 21) {zodiac = "taurus";}
			else if(day > 21 && day <= 31) {zodiac = "gemini";}
			else zodiac = "Please provide correct date";
		} else if (month == 6) {
			if(day >= 1 && day <= 21) {zodiac = "gemini";}
			else if(day > 21 && day <= 30) {zodiac = "cancer";}
			else zodiac = "Please provide correct date";
		} else if (month == 7) {
			if(day >= 1 && day <= 23) {zodiac = "cancer";}
			else if(day > 23 && day <= 30) {zodiac = "leo";}
			else zodiac = "Please provide correct date";
		} else if (month == 8) {
			if(day >= 1 && day <= 23) {zodiac = "leo";}
			else if(day > 23 && day <= 31) {zodiac = "virgo";}
			else zodiac = "Please provide correct date";
		} else if (month == 9) {
			if(day >= 1 && day <= 23) {zodiac = "virgo";}
			else if(day > 23 && day <= 30) {zodiac = "libra";}
			else zodiac = "Please provide correct date";
		} else if (month == 10) {
			if(day >= 1 && day <= 23) {zodiac = "libra";}
			else if(day > 23 && day <= 31) {zodiac = "scorpio";}
			else zodiac = "Please provide correct date";
		} else if (month == 11) {
			if(day >= 1 && day <= 22) {zodiac = "scorpio";}
			else if(day > 22 && day <= 30) {zodiac = "sagittarius";}
			else zodiac = "Please provide correct date";
		} else if (month == 12) {
			if(day >= 1 && day <= 22) {zodiac = "sagittarius";}
			else if(day > 22 && day <= 31) {zodiac = "capricorn";}
			else zodiac = "Please provide correct date";
		} else zodiac = "Please provide correct date";
		
		return zodiac;
	}

	public static void main(String[] args) {
		// HINT: you can use main method to test your getZodiac method with
		// different parameters
		System.out.println("Your zodiac sign is " + getZodiac(3, 45));
	}

}
