package jtm.extra01;

public class GetOne {

	public int iterations(int number) {
		// value until it's
		// reduced to 1.
		// If the number is even then divide it by 2. If it is odd then multiply
		// it by 3 and add 1. Count how many iterations
		// it takes to do this calculation and return that count. For example:
		// passed number is 6. Path to completion would be:
		// 6->3->10->5->16->8->4->2->1. Iteration count=8.
		// HINT: Use while loop.
		
		int iterationCount = 0;
		//int numberToPrint = number;
		
		if(number < 1) {
			System.out.println("Number is not positive");
			return 0;
		}

		while (number > 1) {
			if (number % 2 == 0) {
				number /= 2;
				iterationCount++;
			} else if (number % 2 == 1) {
				number *= 3;
				number++;
				iterationCount++;
			}
		}
		//System.out.println("If the number is " + numberToPrint + ", iterationCount is " + iterationCount);
		return iterationCount;
	}

	public int theMostComplexNo(int maxNumber) {
		// TODO #2: Calculate how many iterations each number from 1 to
		// maxNumber (including) to get value till 1.
		// Return the number, which takes most iterations to do that.
		// E.g. if 3 is passed, then calculate iteration steps for 1, 2 and 3.
		// And return 3, because it has the biggest count of iterations.
		// (If count of iterations is the same for several numbers, return
		// smallest number).
		
		iterations(maxNumber);
		
		int maxIterations = 0;
		int numberToPrint = 0;

		int[] numbers = new int[maxNumber];

		//System.out.println("The start array: ");
		for (int i = 0; i < numbers.length; i++) {// create an array with all numbers from 1 to maxNumber
			numbers[i] = i + 1;
			//System.out.print(numbers[i] + ", ");
		}
		//System.out.println();
		
		for (int number : numbers) {
			if(maxIterations < iterations(number)) {
			maxIterations = iterations(number);
			numberToPrint = number;
			}
		}
		System.out.println("If maxNumber is " + maxNumber + ", number with maxIterations is " + numberToPrint);

		return maxIterations;
	}

	public static void main(String[] args) {

		GetOne reference = new GetOne();
		reference.theMostComplexNo(80);
	}

}