package jtm.extra05;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.json.*;

import com.google.gson.Gson;
import com.mysql.cj.xdevapi.Result;


public class JsonCars {

	/*- TODO #1
	 * Implement method, which returns list of cars from generated JSON string
	 */
	public List<Car> getCars(String jsonString) {
		
		/*- HINTS:
		 * You will need to use:
		 * - https://stleary.github.io/JSON-java/org/json/JSONObject.html
		 * - https://stleary.github.io/JSON-java/org/json/JSONArray.html
		 * You will need to initialize JSON array from "cars" key in JSON string
		 */
				
		
		List<Car> carList = new ArrayList<>();
		JSONObject obj = new JSONObject(jsonString); //convert jsonstring into jsonobject
		JSONArray cars = (JSONArray) obj.get("cars");
		for (int i = 0; i < cars.length(); i++) {
			JSONObject car = (JSONObject) cars.get(i);
			String model = (String) car.get("model");
			int year = (int) car.get("year");
			String color = (String) car.get("color");
			int price = (int) car.get("price");
			Car car2 = new Car(model, year, color, (float) price);
   		    carList.add(car2);
		}		

		return carList;
	}

	/*- TODO #2
	 * Implement method, which returns JSON String generated from list of cars
	 */
	public String getJson(List<Car> cars) {
		/*- HINTS:
		 * You will need to use:
		 * - https://docs.oracle.com/javase/8/docs/api/index.html?java/io/StringWriter.html
		 * - http://static.javadoc.io/org.json/json/20180130/index.html?org/json/JSONWriter.html
		 * Remember to add "car" key as a single container for array of car objects in it.
		 */
		StringWriter stringWriter = new StringWriter();
		JSONWriter writer = new JSONWriter(stringWriter);
		writer.object().key("cars").value(cars).endObject();
		return stringWriter.toString();
	}

}