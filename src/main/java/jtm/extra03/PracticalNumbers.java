package jtm.extra03;

import java.util.ArrayList;
import java.util.Arrays;

public class PracticalNumbers {

	// TODO Read article https://en.wikipedia.org/wiki/Practical_number
	// Implement method, which returns practical numbers in given range
	// including
	public String getPracticalNumbers(int from, int to) {
		
		int a[] = new int[to - from];
		int indx = 0;
		for(int i = from; i < to+1; i++) {
			int sum = 0;
			for(int j = 1; j <= (i/2); j++) {
				if(i % j == 0) {
					if(sum < j - 1) {
						continue;
					}
					sum += j;
				}
			}
			if (sum >= i-1)
				a[indx++] = i;
		}
		int count = 0;
		for(int i = 0; i < a.length; i++) {
			if(a[i] != 0)
				count++;
		}
		int ans[] = new int[count];
		for (int i = 0; i < count; i++) {
			ans[i] = a[i];
		}
		return Arrays.toString(ans);

		/*String practicalNumbers = "";
		int[] numbers = new int[(to - from + 1)]; // declare an array with all integers from-to

		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = from + i;
		}

		for (int i = from; i <= to; i++) { // iterate all numbers from-to
			ArrayList<Integer> dividers = new ArrayList<Integer>(); // declare an arrayList for dividers (will be
																	// separate for each number)
			for (int number : numbers) { // found all dividers for each number from-to
				// if (number >= 1) { // divider should be positive
				if (i % number == 0) { // divider should be less then number
					dividers.add(number); // add dividers into special ArrayList
				}
				for (int j = 1; j < i; j++) {
					if (j == (sum of some dividers))
						practicalNumbers.concat(String.valueOf(i));
				}
				// }
			}
			System.out.println("list of dividers for number " + i + ": " + dividers);
			
			

/*			for (int j = 0; j < dividers.size(); j++) { // how to check if sum of dividers are equal to number? // check all possible sum of dividers 
				for (int divider: dividers) {
					if(divider + dividers.indexOf(j) == i) {
						System.out.println("suitable dividers are " + divider + " and " + dividers.indexOf(j));
						practicalNumbers.concat(String.valueOf(i));
					}
				}
			}*/
				
			 /*
			 * for (int k = j + 1; k < dividers.size(); k++) { for (int l = k + 1; l <
			 * dividers.size(); l++) { if ((j + k) == i || (j + k + l) == i) {
			 * System.out.println("all possibles sums: " + (j + k) + ", " + (j + k + l));
			 * practicalNumbers.concat(String.valueOf(i)); } } } }
			 */

		//}
		// System.out.println(practicalNumbers.toString()); */
		//return practicalNumbers;
	}

	public static void main(String[] args) {
		PracticalNumbers instance = new PracticalNumbers();
		instance.getPracticalNumbers(1, 16);
	}
}