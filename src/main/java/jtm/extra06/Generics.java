package jtm.extra06;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Spliterator;

import static jtm.extra06.GenericsTest.log;

// TODO #1
// import statically jtm.extra06.GenericsTest.log StringBuilder object
// from the closed source unit test
// Note that necessary messages should be written there

public class Generics<E extends Number> extends LinkedList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7708818085602746688L;
	// TODO #2
	// Use Eclipse code generation prompter to add generated serial version ID
	// for this class to avoid warning

	public Generics() {
		super();
		log.append("Generics instance created\n");
		// TODO Auto-generated constructor stub
	}

	// TODO #3
	// Select Source— Generate Constructors from Superclass... and select
	// LinkedList<E>().
	// And implement extended constructor that after new Generics object is
	// created, log has appended line "Generics instance created"
	

	@Override
	public E getFirst() {
		// TODO Auto-generated method stub
		return super.getFirst();
	}

	@Override
	public E getLast() {
		// TODO Auto-generated method stub
		return super.getLast();
	}

	@Override
	public E removeFirst() {
		// TODO Auto-generated method stub
		return super.removeFirst();
	}

	@Override
	public E removeLast() {
		// TODO Auto-generated method stub
		return super.removeLast();
	}

	@Override
	public void addFirst(E e) {
		// TODO Auto-generated method stub
		super.addFirst(e);
	}

	@Override
	public void addLast(E e) {
		// TODO Auto-generated method stub
		super.addLast(e);
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return super.contains(o);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return super.size();
	}

	@Override
	public boolean add(E e) {
		// TODO Auto-generated method stub
		return super.add(e);
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return super.remove(o);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return super.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return super.addAll(index, c);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		super.clear();
	}

	@Override
	public E get(int index) {
		// TODO Auto-generated method stub
		return super.get(index);
	}

	@Override
	public E set(int index, E element) {
		// TODO Auto-generated method stub
		return super.set(index, element);
	}

	@Override
	public void add(int index, E element) {
		// TODO Auto-generated method stub
		super.add(index, element);
	}

	@Override
	public E remove(int index) {
		// TODO Auto-generated method stub
		return super.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return super.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return super.lastIndexOf(o);
	}

	@Override
	public E peek() {
		// TODO Auto-generated method stub
		return super.peek();
	}

	@Override
	public E element() {
		// TODO Auto-generated method stub
		return super.element();
	}

	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return super.poll();
	}

	@Override
	public E remove() {
		// TODO Auto-generated method stub
		return super.remove();
	}

	@Override
	public boolean offer(E e) {
		// TODO Auto-generated method stub
		return super.offer(e);
	}

	@Override
	public boolean offerFirst(E e) {
		// TODO Auto-generated method stub
		return super.offerFirst(e);
	}

	@Override
	public boolean offerLast(E e) {
		// TODO Auto-generated method stub
		return super.offerLast(e);
	}

	@Override
	public E peekFirst() {
		// TODO Auto-generated method stub
		return super.peekFirst();
	}

	@Override
	public E peekLast() {
		// TODO Auto-generated method stub
		return super.peekLast();
	}

	@Override
	public E pollFirst() {
		// TODO Auto-generated method stub
		return super.pollFirst();
	}

	@Override
	public E pollLast() {
		// TODO Auto-generated method stub
		return super.pollLast();
	}

	@Override
	public void push(E e) {
		// TODO Auto-generated method stub
		
		log.append(e.getClass().getName() + ": " + e + " pushed\n");
		super.push(e);
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub

		E element = super.pop();
		log.append(element.getClass().getName() + ": " + element + " popped\n");
		return element;
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		// TODO Auto-generated method stub
		return super.removeFirstOccurrence(o);
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		// TODO Auto-generated method stub
		return super.removeLastOccurrence(o);
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		// TODO Auto-generated method stub
		return super.listIterator(index);
	}

	@Override
	public Iterator<E> descendingIterator() {
		// TODO Auto-generated method stub
		return super.descendingIterator();
	}

	@Override
	public Object clone() {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return super.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return super.toArray(a);
	}

	@Override
	public Spliterator<E> spliterator() {
		// TODO Auto-generated method stub
		return super.spliterator();
	}

	// TODO #4
	// Select menu: Source— Override/Implement methods..., extend LinkedList<E>
	// class, and select push(E) and pop() methods to override them.
	// TODO #4.1
	// Override pop() method of the list, that besides popping element from it
	// log has appended line: "java.lang.Integer: 1 popped", where:
	// java.lang.Integer is its class name, 1 is its actual value
	// HINT:
	// You can use this.peek() method to refer to any object being popped from
	// list
	// TODO #4.2
	// override push() method that besides pushing new element into list
	// log has appended line: "java.lang.Double: 2 pushed", where:
	// java.lang.Double is actual class name of the value
	// 2 is its actual value
}
