package jtm.extra06;

import java.util.ArrayList;
import java.util.List;

/**
 * This enum represents holidays, displayed as month + day value. This enum can
 * give nearest holiday.
 */
public enum Holiday {
	NEW_YEAR(1, 1), WOMAN_DAY(3, 8), CHUCK_NORRIS_BIRTHSDAY(3, 10), FOOLS_DAY9(4, 1), WORLD_END(12, 21);

	int month;
	int day;

	Holiday(int month, int day) {
		this.month = month;
		this.day = day;

		// TODO #1 implement class variables for month and day of the holiday
	}

	public static Holiday getNearest(int currentMonth, int currentDay) {
		Holiday returnHoliday = null;
		// TODO #2 implement method which will return the nearest holiday.
		// HINT: note, that holidays is arranged by date ascending, so if there
		// are
		// no more holidays this year, first holiday in the list will be the
		// next.

		switch (currentMonth) {
		case 1:
			returnHoliday = Holiday.NEW_YEAR;
			break;
		case 2:
			returnHoliday = Holiday.WOMAN_DAY;
			break;
		case 3:
			if (currentDay < 9)
				returnHoliday = Holiday.WOMAN_DAY;
			if (currentDay >= 9 && currentDay <= 20) {
				returnHoliday = Holiday.CHUCK_NORRIS_BIRTHSDAY;
			}
			if (currentDay > 20)
				returnHoliday = Holiday.FOOLS_DAY9;
			break;
		case 4:
			returnHoliday = Holiday.FOOLS_DAY9;
			break;
		case 5:
			returnHoliday = Holiday.FOOLS_DAY9;
			break;
		case 6:
			returnHoliday = Holiday.FOOLS_DAY9;
			break;
		case 7:
			returnHoliday = Holiday.FOOLS_DAY9;
			break;
		case 8:
			if (currentDay < 15)
				returnHoliday = Holiday.FOOLS_DAY9;
			if (currentDay >= 15)
				returnHoliday = Holiday.WORLD_END;
			break;
		case 9:
			returnHoliday = Holiday.WORLD_END;
			break;
		case 10:
			returnHoliday = Holiday.WORLD_END;
			break;
		case 11:
			returnHoliday = Holiday.WORLD_END;
			break;
		case 12:
			if (currentDay < 26)
				returnHoliday = Holiday.WORLD_END;
			if (currentDay >= 26)
				returnHoliday = Holiday.NEW_YEAR;
			break;
		}
		
		return returnHoliday;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}
}
