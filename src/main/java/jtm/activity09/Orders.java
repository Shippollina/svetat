package jtm.activity09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders implements Iterator<Order> {
	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */

	private List<Order> listOfOrders = new ArrayList<Order>();
	private int current = -1;
//	Iterator<Order> listIterator = listOfOrders.iterator();
//	{
//		while (((Orders) listOfOrders).hasNext()) {
//			current += 1;
//		}
//	}

	public Orders() {
		// List<Order> list = new ArrayList<Order>();
		// this.listOfOrders = new ArrayList<Order>();
	}

	public void add(Order item) {
		this.listOfOrders.add(item);
	}

	public List<Order> getItemsList() {
		return this.listOfOrders;
	}

	public Set<Order> getItemsSet() {

		Set setOfOrders = new TreeSet<Order>();
		Order previous = null;
		this.sort();
		for(Order order : this.listOfOrders) {
			if(previous == null) {//for 1st iteration
				previous = order;
				continue;
			}
			if(order.name.equals(previous.name)) {
				previous.count += order.count;
				previous.customer += ", " + order.customer;
			}
			else {
				setOfOrders.add(previous);
				previous = order;
			}
		}
		if(previous != null) //for last iteration
			setOfOrders.add(previous);
//		int counter = 0;
//		for (Order orderInList : listOfOrders) {
//			System.out.println("main for loop counter: " + counter);
//			System.out.println("orderInList: " + orderInList);
//			String name = orderInList.name;
//			Iterator<Order> iterator = setOfOrders.iterator();
//			if (counter == 0) setOfOrders.add(orderInList);
//			int whileCounter = 0;
//			
//			Set setOfOrdersTmp = new TreeSet<Order>();
//			for(Object orderInSet1 : setOfOrders) {
//				System.out.println("while counter: " + whileCounter);
//				Order orderInSet = (Order) orderInSet1;
//				System.out.println("orderInSet: " + orderInSet);
//	
//				if(orderInSet.name == name) {
//					orderInSet.count += orderInList.count;
//					orderInSet.customer = orderInSet.customer.concat(", " + orderInList.customer);
//					System.out.println(setOfOrders + " modyfiying set");
//				} else {
//					setOfOrders.add(orderInList);
//					System.out.println(setOfOrders + " adding to set");
//				}
//				whileCounter ++;
//			}
//			counter += 1;

					
			// to unit entries with common item
			
//			if (!(theOrder.equals(previous))) {
//				for (Order anOrder : listOfOrders) {
//					if (theOrder != anOrder && theOrder != previous && theOrder.name.matches(anOrder.name)) {
//						previous.name = theOrder.name;
//						previous.count = theOrder.count + anOrder.count;
//						if (theOrder.customer != anOrder.customer) {
//							previous.customer = theOrder.customer.concat(", " + anOrder.customer);
//						}
//						setOfOrders.add(previous);
//					}
//
//				}
//			}
//		}
		return setOfOrders;

	}

	// @Override
	public void sort() {
		this.listOfOrders.sort(Comparator.naturalOrder());
		// Collections.sort(listOfOrders);
	}

	@Override
	public boolean hasNext() {
		if (this.listOfOrders.size() > 0 && current + 1 < this.listOfOrders.size()) {
			return true;
		} else
			return false;
	}

	@Override
	public Order next() {// throws NoSuchElementException {

		Order someOrder = null;
		if (this.hasNext()) {
			someOrder = listOfOrders.get(current + 1);
			//this.current++;
		} else
			throw new NoSuchElementException();
		return someOrder;
	}

	@Override
	public void remove() {// throws IllegalStateException {
		if(this.current >= 0) {
			this.listOfOrders.remove(this.current);
			this.current--;
		}
		else
			throw new IllegalStateException();
//		try {
//			this.listOfOrders.remove(this.next());
//		} catch (NoSuchElementException e) {
//			throw new IllegalStateException();
//		}
//		}
	}

	@Override
	public String toString() {
		return this.listOfOrders.toString();
	}

	public static void main(String[] args) {
		Orders allOrders = new Orders();
		allOrders.add(new Order("mom", "box", 3));
		allOrders.add(new Order("mom", "bird", 4));
		allOrders.add(new Order("dad", "bird", 4));
		allOrders.add(new Order("granny", "box", 4));
		allOrders.add(new Order("son", "box", 1));
		System.out.println(allOrders.toString());
		System.out.println(allOrders.getItemsSet());
	}

}
