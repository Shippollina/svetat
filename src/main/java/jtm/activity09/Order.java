package jtm.activity09;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection 
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

public class Order implements Comparable<Order> {
	String customer; // Name of the customer
	String name; // Name of the requested item
	int count; // Count of the requested items

	public Order(String orderer, String itemName, Integer count) {
		this.customer = orderer;
		this.name = itemName;
		this.count = count;
	}

	@Override
	public int compareTo(Order o) {

		int tempItemName = 0;
		int tempCustomer = 0;
		int tempCount = 0;

		// compare itemNames

		if (this.name == null && o.name == null)
			tempItemName = 0;
		else if (this.name == null) {
			tempItemName = -1;
		} else if (o.name == null) {
			tempItemName = 1;
		} else {
			tempItemName = this.name.compareTo(o.name);
		}

		if (tempItemName == 0) {
			// compare customers
			if (this.customer == null && o.customer == null)
				tempCustomer = 0;
			else if (this.customer == null) {
				tempCustomer = -1;
			} else if (o.customer == null) {
				tempCustomer = 1;
			} else {
				tempCustomer = this.customer.compareTo(o.customer);
			}
		}

		if (tempItemName == 0 && tempCustomer == 0) {
			// compare count
			if (this.count == 0 && o.count == 0)
				tempCount = 0;
			else if (this.count == 0) {
				tempCount = -1;
			} else if (o.count == 0) {
				tempCount = 1;
			} else {
				tempCount = this.count - o.count;
			}
		}
		
		if (tempItemName > 1)
			tempItemName = 1;
		if(tempItemName < -1)
			tempItemName = -1;
		if(tempCustomer > 1)
			tempCustomer = 1;
		if(tempCustomer < -1)
			tempCustomer = -1;

		if (tempItemName != 0)
			return tempItemName;
		else if (tempCustomer != 0)
			return tempCustomer;
		else
			return tempCount;
	}

	@Override
	public int hashCode() {
		return Objects.hash(count, customer, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
		//if(this.name == obj.name && this.count == obj.count && this.customer == obj.customer)
			return true;
		}
		if (!(obj instanceof Order)) {
			return false;
		}
		Order other = (Order) obj;
		
		return count == other.count && Objects.equals(customer, other.customer) && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String message = this.name + ": " + this.customer + ": " + this.count;
		return message;
	}

	public static void main(String[] args) {
		Order myOrder = new Order("Anna", "box", 3);
		Order hisOrder = new Order("Anna", "boxy", 3);
		System.out.println(myOrder.compareTo(hisOrder));
		if (myOrder.compareTo(hisOrder) == 0)
			System.out.println("equals");
		else if (myOrder.compareTo(hisOrder) > 0)
			System.out.println("MyOrder is greater");
		else if(myOrder.compareTo(hisOrder) < 0)
			System.out.println("MyOrder is less");

	}

}
