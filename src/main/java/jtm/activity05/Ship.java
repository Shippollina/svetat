package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport{

	protected byte sailes;
	
	
	
	public Ship (String id, byte sailes) {
		super(id, 0, 0);
		this.sailes = sailes;
	}

	@Override
	public String move(Road road) {

		//to return String in form:
		//ID Ship is sailing on (Road as String) with x sails
		//return Cannot sail on (Road as String) if it is not WaterRoad.
		
		if (!(road instanceof WaterRoad)) {
			return "Cannot sail on " + road.toString();
		}
		return this.getType() + " is sailing on " + road.toString() + " with "
		+ this.sailes + " sails";
	}
	
	public static void main(String[] args) {
		Ship theShip = new Ship("AA", (byte)7);
		System.out.println(theShip.move(new WaterRoad("A", "B", 45)));
	}
}
