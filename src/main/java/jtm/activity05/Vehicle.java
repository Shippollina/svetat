package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Vehicle extends Transport {

protected int numberOfWheels; 
	
	public Vehicle(String id, float consumption, int tankSize, int wheels){
		super(id, consumption, tankSize);
		this.numberOfWheels = wheels;
	}


	@Override
	public String move(Road road) {
		
		//ID Vehicle is driving on (Road as String) with x wheels
		//return Cannot drive on (Road as String) if it is not Road.
		
		if(!(road instanceof Road)) {
			return "Cannot drive on " + road.toString();
		}
		return this.getType() + " is driving on " + road + 
				" with " + this.numberOfWheels + " wheels";
	}
	
//	public static void main(String[] args) {
//		Vehicle theVehicle = new Vehicle("ASA", 4, 10, 4);
//		System.out.println(theVehicle.move(new Road("X", "Y", 100)));
//		System.out.println(theVehicle.getConsumption());
//		
//	}
	

}
