package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {

	private Ship ship;
	private Vehicle vehicle;
	
	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels){
		super(id, consumption, tankSize);
		ship = new Ship(getId(), sails);
		vehicle = new Vehicle(getId(), getConsumption(), getTankSize(), wheels);
	}

	@Override
	public String move(Road road) {
		// Amhibia behaves like a Vehicle on ground road and like a Ship on water road
		
		if(road instanceof Road) {
			return vehicle.move(road).replace("Vehicle", "Amphibia");
		}else {
			return ship.move(road).replace("Ship", "Amphibia");		
		}
	}
	
//	public static void main(String[] args) {
//		Amphibia theAmphibia = new Amphibia("RTY", 34, 15, (byte)7, 0);
//		System.out.println(theAmphibia.move(new WaterRoad("E", "4", 56)));
//	}
}
